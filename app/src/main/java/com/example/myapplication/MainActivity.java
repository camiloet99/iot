package com.example.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private int edad;
    private double contam;
    private float latitud, longitud;
    private TextView dirTxt, lat, lon, aqiview, aviso, recomendacion;
    private List<Address> direccion;
    private String dir, url, link, nivelaqi, avisoCont, auxtxt, recomtxt;
    private Button boton;
    private EditText edadTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck== PackageManager.PERMISSION_DENIED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){ }
            else ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
        dirTxt = (TextView) findViewById(R.id.direccionTxt);
        boton = (Button)findViewById(R.id.btnact);
        lat = (TextView)findViewById(R.id.latTxt);
        lon = (TextView)findViewById(R.id.longTxt);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generarConsulta();
            }
        });
        edadTxt = (EditText)findViewById(R.id.edadTxt);
        aqiview = (TextView)findViewById(R.id.aqiTxt);
        aviso = (TextView)findViewById(R.id.avisoTxt);
        recomendacion = (TextView)findViewById(R.id.recomTxt);
    }

    public void recibirGps(){
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck==PackageManager.PERMISSION_DENIED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            } else ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        }
    }

    public void generarConsulta(){
        edad = Integer.parseInt(edadTxt.getText().toString());
        try{
            if(edad>0 && edad<110){
                url = agregarCoordenadas();
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                JsonObjectRequest objectRequest = new JsonObjectRequest(
                        Request.Method.GET, url, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                JSONObject aqiJson = response;
                                try {
                                    nivelaqi = aqiJson.getString("AQI");
                                    aqiview.setText(nivelaqi);
                                    contam  = calcularContaminacion(Double.valueOf(nivelaqi));
                                    if(contam>=0 && contam<=50) auxtxt = "Nivel de contaminacion Bueno, con un AQI de: "+Double.toString(contam);
                                    else if(contam>50 && contam<=100) auxtxt = "Nivel de contaminacion Moderado, con un AQI de: "+Double.toString(contam);
                                    else if(contam>100 && contam<=150) auxtxt = "Nivel de contaminacion Insalubre para grupos sensibles, con un AQI de: "+Double.toString(contam);
                                    else if(contam>150 && contam<=200) auxtxt = "Nivel de contaminacion Insalubre, con un AQI de: "+Double.toString(contam);
                                    else if(contam>200 && contam<=300) auxtxt = "Nivel de contaminacion Muy poco saludable, con un AQI de: "+Double.toString(contam);
                                    else if(contam>300 && contam<=500) auxtxt = "Nivel de contaminacion Peligroso, con un AQI de: "+Double.toString(contam);
                                    aviso.setText(auxtxt);
                                    if(contam>=0 && contam<=100) recomtxt = "No importa su edad, el nivel de contaminacion es adecuado para que salga a hacer lo que quiera!";
                                    else if(contam>100 && contam<=200 && edad <= 50) recomtxt = "El nivel de contaminacion le permite salir a hacer lo que desee, pero solo debido a su edad";
                                    else if(contam>100 && contam<=200 && edad >50) recomtxt = "Las personas en su rango de edad no deberian salir, es peligroso para su salud";
                                    else if(contam>200) recomtxt = "La contaminacion esta demasiado elevada, no se le recomienda salir en ningun caso";
                                    recomendacion.setText(recomtxt);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(),  error.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                );
                requestQueue.add(objectRequest);
            }else Toast.makeText(getApplicationContext(), "Ingrese una edad verdadera", Toast.LENGTH_SHORT).show();
        }catch (Exception e){Toast.makeText(getApplicationContext(), "Ingrese una edad", Toast.LENGTH_SHORT).show();}
    }

    public String agregarCoordenadas(){
        LocationManager locationManager = (LocationManager) MainActivity.this.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener(){
            public void onLocationChanged(Location location){
                try {
                    Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                    direccion = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    dir = direccion.get(0).getAddressLine(0);
                }catch (IOException e) {}
                dirTxt.setText(dir.toString());
                latitud = (float)location.getLatitude();
                lat.setText(Float.toString(latitud));
                longitud = (float)location.getLongitude();
                lon.setText(Float.toString(longitud));
                link = "http://ec2-52-23-227-173.compute-1.amazonaws.com/aqi/"+Float.toString(longitud)+"/"+Float.toString(latitud);
                //link = "http://ec2-52-23-227-173.compute-1.amazonaws.com/aqi/-75.5875898/6.2452559";
            }
            public void onStatusChanged(String provider, int status, Bundle extras) {}
            public void onProviderEnabled(String provider){}
            public void onProviderDisabled(String provider) {}
        };
        int permissionCheck = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,locationListener);
        return link;
    }

    public double calcularContaminacion(double aqi){
        double cont=0;
        if(aqi>0 && aqi<12) cont = (4.16)*aqi;
        else if(aqi>12.1 && aqi<35.4) cont = (2.14)*(aqi-12.1)+51;
        else if(aqi>35.5 && aqi<55.4) cont = (2.46)*(aqi-35.5)+101;
        else if(aqi>55.5 && aqi<150.4) cont = (0.51)*(aqi-55.5)+151;
        else if(aqi>150.5 && aqi<250.4) cont = (0.99)*(aqi-150.5)+201;
        else if(aqi>250.5) cont = (0.80)*(aqi-250.5)+301;
        return cont;
    }
}